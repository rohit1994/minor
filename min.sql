-- phpMyAdmin SQL Dump
-- version 4.2.12deb2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 05, 2015 at 11:54 PM
-- Server version: 5.6.27-0ubuntu0.15.04.1
-- PHP Version: 5.6.4-4ubuntu6.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `min`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `user_name` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`user_name`, `password`) VALUES
('mittal', 'rohit');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(100) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'Fashion'),
(2, 'Mobile\r\n'),
(3, 'Furniture'),
(4, 'Health'),
(5, 'Tv & Gaming'),
(6, 'Sports\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
`id` int(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `subject` varchar(5000) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `name`, `email`, `subject`) VALUES
(1, 'shubham mittal', 'shubham.mital.kv1@gmail.com', ' we are very happy with this product chutiye rohit');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
`id` int(200) NOT NULL,
  `product_id` varchar(100) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `feed` varchar(1000) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `product_id`, `name`, `email`, `feed`) VALUES
(2, '2', 'Rohit Yadav', 'apnamail@gmail.com', 'my name is shubham '),
(3, '2', 'shubham mittal', 'mittal@pagal.com', 'my name is shubham '),
(4, '2', 'Rohit Yadav', 'apnamail@gmail.com', 'my name is shubham '),
(5, '2', 'shubham mittal', 'mittal@pagal.com', 'my name is shubham '),
(6, '2', 'Rohit Yadav', 'ab@nahi.com', 'ab to chal ja'),
(7, '2', 'Rohit Yadav', 'ab@nahi.com', 'my name is shubham '),
(10, '2', 'Rohit Yadav', 'mittal@pagal.com', 'my name is shubham '),
(11, '2', 'shubham mittal', 'ab@nahi.com', 'my name is shubham '),
(12, '5', 'shubham mittal', 'shubham.mittal.kv1@gmail.com', 'my name is shubham ');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE IF NOT EXISTS `locations` (
`id` int(100) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `name`) VALUES
(1, 'Pilani'),
(2, 'Bkbiet'),
(3, 'Jaipur'),
(4, 'Chirawa');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
`id` int(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `brand` varchar(100) NOT NULL,
  `category` varchar(100) NOT NULL,
  `shop_name` varchar(100) NOT NULL,
  `price` int(100) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `item_image` varchar(1000) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `brand`, `category`, `shop_name`, `price`, `description`, `item_image`) VALUES
(1, 'Men Jeans', 'Adidas', 'Fashion', 'Sant Shree', 1500, 'Awesome jeans with perfrct fitting', 'file_upload/1.png'),
(2, 'T-shirt', 'Nike', 'Mobile\r\n', 'Lamba Towers', 2555, 'T-shirt le lo jaldi se bik jaayegi warna', 'file_upload/2.png'),
(3, 'Kurti', 'Calvin', 'Fashion', 'Lamba Towers', 800, 'Kurti h bhai', 'file_upload/3.jpg'),
(4, 'Top', 'American Swan', 'Furniture', 'Lamba Towers', 200, 'Top h bhai lena h to bol', 'file_upload/4.jpg'),
(5, 'T-shirt', 'Wrangler', 'Tv & Gaming', 'Sant Shree', 2366, '80% off h bhai lle', 'file_upload/5.png'),
(6, 'Chair', 'Random', 'Furniture', 'Sant Shree', 5000, 'White chair very cheap', 'file_upload/6.jpg'),
(7, 'Galaxy s6', 'Samsung', 'Mobile\r\n', 'Sant Shree', 50000, 'Aukat se bahar phone', 'file_upload/7.jpg'),
(8, 'camera', 'Nikon', 'Mobile\r\n', 'Lamba Towers', 20000, 'kala camera', 'file_upload/8.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE IF NOT EXISTS `review` (
`id` int(200) NOT NULL,
  `pid` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  `rev` varchar(5000) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`id`, `pid`, `name`, `rev`) VALUES
(1, '2', 'mittal shubham', 'what a lovely product'),
(2, '2', 'Rohit yadav', 'kyaa sahi product h yaar');

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE IF NOT EXISTS `shops` (
`id` int(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `g_map` varchar(1000) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`id`, `name`, `address`, `phone`, `location`, `g_map`) VALUES
(1, 'Sant Shree', 'BKBIET, Pilani', '9865321452', 'Pilani', 'file_upload/map.jpg'),
(2, 'Lamba Towers', 'Opp. BKBIET, Pilani', '8965412365', 'Bkbiet', 'file_upload/map.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shops`
--
ALTER TABLE `shops`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
MODIFY `id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
MODIFY `id` int(200) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
MODIFY `id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
MODIFY `id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
MODIFY `id` int(200) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `shops`
--
ALTER TABLE `shops`
MODIFY `id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
