<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Admin-homepage</title>
     <?php
    SESSION_START();
if($_SESSION['adm'])
{
    $get=$_SESSION['adm'];
}
else
{   
   
    header("location:admin.php");
} ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="admin/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="admin/css/bootstrap.css"/>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="admin/css/jobposting.css">
  
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-fixed-top navbar-default" role="navigation" style="background-color= #003366">
    <div class="container"><!-- Wrap navbar in a responsive container -->
        <div class="navbar-header"><!-- Brand and toggle get grouped for better mobile display -->
            
            <a class="navbar-brand" style="font-family: jobninjafont; font-size: 1.75em" href="#">See Shop</a>
        </div>
        
        <div id="hea">
        <button type="button" class="btn btn-danger">Log out</button>
        </div>
    </div>
</nav>
<div id="ten">
 <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-users fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            
                            <div id="qe"><h1>ADD</h1></div>
                        </div>
                    </div>
                </div>
                <a href="add1.php">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>

</div>
<div class="col-lg-3 col-md-6">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-user fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <a href="update1.php">
                                <div id="qe"><h1>UPDATE</h1></div>
                        </div>
                    </div>
                </div>
                <a href="employeeslist.php">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-user fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <a href="delete1.php">
                                <div id="qe"><h1>DELETE</h1></div>
                        </div>
                    </div>
                </div>
                <a href="employeeslist.php">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-user fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <a href="employeeslist.php">
                                <div id="qe"><h1>SHOW</h1></div>
                        </div>
                    </div>
                </div>
                <a href="employeeslist.php">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
</body>
</html>