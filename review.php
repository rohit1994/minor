<!--A Design by W3layouts 
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>Contact Us</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Custom Theme files -->
<!--theme-style-->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />	
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Markito Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href='http://fonts.googleapis.com/css?family=Amaranth:400,700' rel='stylesheet' type='text/css'>
<!--//fonts-->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
					jQuery(document).ready(function($) {
						$(".scroll").click(function(event){		
							event.preventDefault();
							$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
						});
					});
				</script>
<!-- start menu -->
<link href="css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="js/megamenu.js"></script>
<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>

<script src="js/simpleCart.min.js"> </script>
</head>
<body> 
<!--header-->	
<div class="header" >
	<div class="top-header" >		
		<div class="container">
		<div class="top-head" >
		
			<div class="header-para">
				<a href="main.php"><img src="images/mlogo.png" alt="" ></a>		
			</div>	
			
			
			</div>
	
	<div class="head-top">
			
		<div class="top-nav">		
			  <ul class="megamenu skyblue">
				      <li class="active grid"><a  href="products.php?cat=Fashion">fashion</a>
					    <div class="megapanel">
						<div class="row">
							<div class="col1">
								<div class="h_nav">
									<ul>
										<li><a href="products.php?cat=Fashion">Accessories</a></li>
										
									</ul>	
								</div>							
							</div>
							<div class="col1">
								<div class="h_nav">
									<ul>
										<li><a href="products.php?cat=Fashion">Shirts</a></li>
										
									</ul>	
								</div>							
							</div>
							<div class="col1">
								<div class="h_nav">
									<h4>Popular Brands</h4>
									<ul>
										<li><a href="products.php?cat=Fashion">Levis</a></li>
										
									</ul>	
								</div>												
							</div>
						  </div>
						</div>
					</li>
					<li><a   href="products.php?cat=Furniture">furniture & decor</a>
					 <div class="megapanel">
						<div class="row">
							<div class="col1">
								<div class="h_nav">
									<ul>
										<li><a href="products.php?cat=Furniture">Bed</a></li>
										
								</div>							
							</div>
							<div class="col1">
								<div class="h_nav">
									<ul>
										<li><a href="products.php?cat=Furniture">Armchair</a></li>
										
									</ul>	
								</div>							
							</div>
							<div class="col1">
								<div class="h_nav">
									<h4>Popular Brands</h4>
									<ul>
										<li><a href="products.php?cat=Furniture">Levis</a></li>
										
									</ul>	
								</div>												
							</div>
						</div></li>
				    <li class="grid"><a  href="products.php?cat=Mobile">mobiles & tablets</a>
					   <div class="megapanel">
						<div class="row">
							<div class="col1">
								<div class="h_nav">
									<ul>
										<li><a href="products.php?cat=Mobile">Lenovo Tablets</a></li>
										
									</ul>	
								</div>							
							</div>
							<div class="col1">
								<div class="h_nav">
									<ul>
										<li><a href="products.php?cat=Mobile">Asus Zenfone 2</a></li>
										
									</ul>	
								</div>							
							</div>
							<div class="col1">
								<div class="h_nav">
									<h4>Popular Brands</h4>
									<ul>
										<li><a href="products.php?cat=Mobile">Levis</a></li>
										
									</ul>	
								</div>												
							</div>
						  </div>
						</div>
			    </li>
						<li class="grid"><a  href="products.php?cat=Health">health & beauty</a>
					   <div class="megapanel">
						<div class="row">
							<div class="col1">
								<div class="h_nav">
									<ul>
										<li><a href="products.php?cat=Health">Lenovo Tablets</a></li>
										
									</ul>	
								</div>							
							</div>
							<div class="col1">
								<div class="h_nav">
									<ul>
										<li><a href="products.php?cat=Health">Asus Zenfone 2</a></li>
										
									</ul>	
								</div>							
							</div>
							<div class="col1">
								<div class="h_nav">
									<h4>Popular Brands</h4>
									<ul>
										<li><a href="products.php?cat=Health">Levis</a></li>
										
									</ul>	
								</div>												
							</div>
						  </div>
						</div>
			    </li>		
				<li><a  href="products.php?cat=Tv & Gaming">tv's</a>
				  <div class="megapanel">
						<div class="row">
							<div class="col1">
								<div class="h_nav">
									<ul>
										<li><a href="products.php?cat=Tv & Gaming">Lenovo Tablets</a></li>
										
									</ul>	
								</div>							
							</div>
							<div class="col1">
								<div class="h_nav">
									<ul>
										<li><a href="products.php?cat=Tv & Gaming">Asus Zenfone 2</a></li>
										
									</ul>	
								</div>							
							</div>
							<div class="col1">
								<div class="h_nav">
									<h4>Popular Brands</h4>
									<ul>
										<li><a href="products.php?cat=Tv & Gaming">Levis</a></li>
										
									</ul>	
								</div>												
							</div>
						  </div>
						</div></li>
						<li><a  href="404.php">cameras</a>
				  <div class="megapanel">
						<div class="row">
							<div class="col1">
								<div class="h_nav">
									<ul>
										<li><a href="404.php">Lenovo Tablets</a></li>
										
									</ul>	
								</div>							
							</div>
							<div class="col1">
								<div class="h_nav">
									<ul>
										<li><a href="404.php">Asus Zenfone 2</a></li>
										
									</ul>	
								</div>							
							</div>
							<div class="col1">
								<div class="h_nav">
									<h4>Popular Brands</h4>
									<ul>
										<li><a href="404.php">Levis</a></li>
										
									</ul>	
								</div>												
							</div>
						  </div>
						</div></li> 	
			
				<li><a  href="products.php?cat=Sports">Sports</a>
				  <div class="megapanel">
						<div class="row">
							<div class="col1">
								<div class="h_nav">
									<ul>
										<li><a href="products.php?cat=Sports">Lenovo Tablets</a></li>
										
									</ul>	
								</div>							
							</div>
							<div class="col1">
								<div class="h_nav">
									<ul>
										<li><a href="products.php?cat=Sports">Asus Zenfone 2</a></li>
										
									</ul>	
								</div>							
							</div>
							<div class="col1">
								<div class="h_nav">
									<h4>Popular Brands</h4>
									<ul>
										<li><a href="products.php?cat=Sports">Levis</a></li>
										
									</ul>	
								</div>												
							</div>
						  </div>
						</div></li>
						<li class="grid"><a  href="404.php">toy's & gifts</a>
					   <div class="megapanel">
						<div class="row">
							<div class="col1">
								<div class="h_nav">
									<ul>
										<li><a href="404.php">Lenovo Tablets</a></li>
										
									</ul>	
								</div>							
							</div>
							<div class="col1">
								<div class="h_nav">
									<ul>
										<li><a href="404.php">Asus Zenfone 2</a></li>
									
									</ul>	
								</div>							
							</div>
							<div class="col1">
								<div class="h_nav">
									<h4>Popular Brands</h4>
									<ul>
										<li><a href="404.php">Levis</a></li>
										
									</ul>	
								</div>												
							</div>
						  </div>
						</div>
			    </li>		
			
			
			  </ul> 
				
				<div class="clearfix"> </div>
			</div>
				<div class="clearfix"> </div>
		</div>
		</div>
	</div>
		<!---->
	
		<div class="header-top">
		
<div id="ser">
		 <div class="col-lg-12">
    <div class="input-group1">
    	  <form action="aftersearch.php" method="get" >
      <select class="form-control" name="sho">
      	<option >choose shop</option>
  <?php 
include("db.php");

$eee=mysql_query("select * from shops ");
while($ee=mysql_fetch_array($eee))
{?>
        <option value="<?php echo $ee['name'];?>"><?php echo $ee['name']; ?></option>
                                        
                                        <?php
}
?>
</select> 
<div id="ru">
<span class="input-group-btn" id="sizing-addon1">
        <input type="submit" class="btn btn-success" value="Search"/>
</div>
      
         <?php

{      
  $sho=$_POST['sho'];

    
     }
     ?>

    </form>
      </span>
    </div><!-- /input-group -->
  </div>
	</div>
	<div id="loc">
	<div class="col-lg">
    <div class="input-group">
      <form action="searchp.php" method="get">
      <select name="pri" class="form-control">
      	<option >choose price</option>
  <option value="500">Below 500</option>
  <option value="1000">500-1000</option>
  <option value="2000">1000-2000</option>
  <option value="5000">2000-5000</option>
  <option value="1000000">more than 5000</option>
</select>
      <div class="input-group-btn" id="sizing-addon1">
        <input type="submit" class="btn btn-warning" value="Filter"/>
        </form>
      </div><!-- /btn-group -->
    </div><!-- /input-group -->
  </div><!-- /.col-lg-6 -->
</div>

 


				</div>
					
		
				<div class="clearfix"> </div>
		</div>
		</div>
	</div>
</div>

	<div class="container">
		<div class="contact">
	<h2>Write Your Review</h2>
				<div class="contact-in">
				
				<div class=" col-md-9 contact-left">
				  <?php
	  $pid=$_GET['pro'];
	  ?>
					    <form action="" method="post">
					    	<div>
						    	<span>Name</span>
						    	<input  type="text" name="user" class="textbox">
						    </div>
						    <div>
						    	<span>E-Mail</span>
						    	<input  type="text" name="mail" class="textbox">
						    	
						    </div>
						    <div>
						    	<span>Your Review</span>
						    	<textarea name="usermsg">my name is shubham </textarea>
						    </div>
						    
						   <div id="ra">
						   		<input type="submit" name="sub" value="Submit">
								</div>
						  </div>


						  <?php
include("db.php");
if(isset($_POST['sub']))
{

  
  
  $name=$_POST['user'];
  $mail=$_POST['mail'];
  
  $msg=$_POST['usermsg'];
 

    $z="insert into feedback values('','$pid','$name','$mail','$msg')";
    $m=mysql_query($z);
     ?>
    <script type="text/javascript">

alert("review submitted successfully, your review is under process will be posted soon!!! thank you");
window.location.href = "single.php?q=<?php echo $pid ?>";
</script>
<?php   
}
?>


					    </form>
				  </div>
<div class="col-md-3 at-single">
					<div class="single-bottom">
					<q>More Products</q>
							<div class="product-go">
								<img class="img-responsive fashion" src="images/fa.jpg" alt="">
							<div class="grid-product">
								<a href="#" class="elit">Consectetuer adipiscing elit</a>
								<span class=" price-in"><small>Rs 500.00</small> Rs400.00</span>
							</div>
							<div class="clearfix"> </div>
							</div>
							<div class="product-go">
								<img class="img-responsive fashion" src="images/fa1.jpg" alt="">
							<div class="grid-product">
								<a href="#" class="elit">Consectetuer adipiscing elit</a>
								<span class=" price-in"><small>Rs 500.00</small> Rs400.00</span>
							</div>
							<div class="clearfix"> </div>
							</div>
							<div class="product-go">
								<img class="img-responsive fashion" src="images/fa2.jpg" alt="">
							<div class="grid-product">
								<a href="#" class="elit">Consectetuer adipiscing elit</a>
								<span class=" price-in"><small>Rs 500.00</small> Rs400.00</span>
							</div>
							<div class="clearfix"> </div>
							</div>
							<div class="product-go">
								<img class="img-responsive fashion" src="images/fa1.jpg" alt="">
							<div class="grid-product">
								<a href="#" class="elit">Consectetuer adipiscing elit</a>
								<span class=" price-in"><small>Rs 500.00</small> Rs400.00</span>
							</div>
							<div class="clearfix"> </div>
							</div>
							
							
				</div>
				</div>
				<div class="clearfix"> </div>		
				</div>
					
					
				<div class="clearfix"> </div>		
		</div>


	</div>
</div>
					
					  <div class="clearfix"></div>
				 </div>
			    
      		</div>
      		

	</div>
		
		</div>

</div>
	<!--footer-->
	<div class="footer">
		<div class="container">
			<div class="col-md-3 footer-left">
				<a href="index.html"><img src="images/rsz_s2e.png" alt=""></a>
				<p class="footer-class">© Made By SHUBHAM MITTAL </p>
			</div>
			<div class="col-md-2 footer-middle">
				<ul>
					<li><a href="404.php">about us</a> </li>
					<li><a href="contact.php">   contact us</a></li>
					<li ><a href="404.php" >  our stores</a></li>
				</ul>
			</div>
			<div class="col-md-4 footer-left-in">
				<ul class="term">
					<li><a href="#">terms and conditions</a> </li>
					<li><a href="#">  See Shop in the press</a></li>
					<li ><a href="#" >  testimonials</a></li>	
				</ul>
				<ul class="term">
					<li><a href="#">join us</a> </li>
					<li><a href="#">  See Shop videos</a></li>
					
				</ul>
				<div class="clearfix"> </div>
			</div>
			
			<div class="clearfix"> </div>
		</div>
		<script type="text/javascript">
						$(document).ready(function() {
							/*
							var defaults = {
					  			containerID: 'toTop', // fading element id
								containerHoverID: 'toTopHover', // fading element hover id
								scrollSpeed: 1200,
								easingType: 'linear' 
					 		};
							*/
							
							$().UItoTop({ easingType: 'easeOutQuart' });
							
						});
					</script>
				<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

	</div>
</body>
</html>